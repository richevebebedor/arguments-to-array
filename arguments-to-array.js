/*:
	@module-configuration:
		{
			"packageName": "arguments-to-array",
			"fileName": "arguments-to-array.js",
			"moduleName": "argumentsToArray",
			"authorName": "Richeve S. Bebedor",
			"authorEMail": "richeve.bebedor@gmail.com",
			"repository": "git@github.com:volkovasystems/arguments-to-array.git",
			"isGlobal": true
		}
	@end-module-configuration

	@module-documentation:

	@end-module-documentation

	@include:
	@end-include
*/
var argumentsToArray = function argumentsToArray( parameterList ){
	/*:
		@meta-configuration:
			{
				"parameterList:required": "Arguments"
			}
		@end-meta-configuration
	*/

	return Array.prototype.slice.call( parameterList );
};

( module || { } ).exports = argumentsToArray;
